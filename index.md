---
title: Welcome to Doczilla
description: Take a few minutes to learn about Doczilla.
category: Introduction
tags:
  - Doczilla
  - welcome
---

# {{ $frontmatter.title }}

[[toc]]

## What is It?

Doczilla is a starter template for Vitepress, nothing more. So instead of installing Vitepress into your application for your docs, you'll clone this repository.

```zsh
git clone https://gitlab.com/zenphp/doczilla dz

cd dz && npm install
```

> More on this in [installation](./installation)

## Why Did We Make It?

Our primary application framework [ZenPHP](https://zenphp.org) is a fork of [Laravel,](https://laravel.com) for modular development. It uses [Vue 3](https://vuejs.org) through [Inertia.js](https://inertiajs.com) and [TailwindCSS](https://tailwindcss.com) exclusively for the frontend. We needed a documentation package that supported both Vue 3, and Tailwind.

Since Vitepress doesn't support Tailwind, Doczilla was born.

## Who Is It For?

For anyone wanting a beautiful and completely editable documentation system built with TailwindCSS. The truly unique part of Doczilla, it that it ships with all the components right in your `.vitepress/theme` directory, so you have complete control of how they look, or what they display.

Gone are the days of having all the components tucked away in the `node_modules` directory where you can't edit them. We've done all the work of extracting what's needed and giving control back to you.
